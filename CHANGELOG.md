<!-- markdownlint-configure-file { "MD024": false } -->
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.2-dev0] - 2025-03-03

### Changed

- runtime 3.10.15
- allow pieces to be external url

## [0.3.1-dev0] - 2025-02-19

### Changed

- TrackIt links open in new tab

## [0.3.0-dev1] - 2025-02-19

### Fixed

- actually call the new _ensure_ordered function

## [0.3.0-dev0] - 2025-02-19

### Changed

- feat_fill_ordered

## [0.2.3-dev0] - 2024-08-07

### Changed

- New favicon
- Updated safety-policy

## [0.2.2] - 2024-06-24

### Fixed

- Item edit

## [0.2.1] - 2024-06-19

### Changed

- Improved health check
- Added tests for all app routes

### Fixed

- Disabled order_id field in item edit page

## 0.2.0 - 2024-06-18

### Added

- Support editing items

## 0.1.0 - 2024-06-16

- _First release._
